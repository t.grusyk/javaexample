import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class Main {
    private int num;


    public static void main(String[] args) throws IOException {
        IfElse ifElse = new IfElse();
        SwitchCase switchCase = new SwitchCase();
        Numbers numbers = new Numbers();
       // MethodToTestConstruktor methodToTestConstruktor = new MethodToTestConstruktor();

// if else
        System.out.println("\t\tIF / Else:  ");
        ifElse.justIf();
        ifElse.ifElse();
        ifElse.ifElseIf();
        System.out.println();
//switch / case
        System.out.println("\t\tSwitch / Case ( 1,2,3): ввели - 2 ");
        switchCase.switchCase(2);
        System.out.println("\t\tSwitch / Case ( 1,2,3): ввели - 5 ");
        switchCase.switchCase(5);
//numbers
        System.out.println("\t\tNumbers");
        numbers.addTwoNumbers();
        System.out.println(numbers.numbersCheck());
        System.out.print("Пишем аргументи x=%d y=%d потім через кому x,y = ");
        numbers.arguments();

        MethodToTestConstruktor taras = new MethodToTestConstruktor();
        MethodToTestConstruktor hrysiuk = new MethodToTestConstruktor("Hrysiuk");
        MethodToTestConstruktor test = new MethodToTestConstruktor("Hrysiuk", 22);
        taras.prinInfo();
        hrysiuk.prinInfo();
        taras.prinInfo();


//        Reader();
//        if (Reader() == 1){
//            ifElse.ifElse();
//        }
    }

//    private static int Reader() throws IOException {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String number = reader.readLine();
//        int num = Integer.parseInt(number);
//        return num;
//    }
}
