public class SwitchCase {
    public void switchCase( int num) {
        switch (num){
            case 1:
                System.out.println("Варіант якщо ви ввели 1");
                break;
            case 2:
                System.out.println("Варіант якщо ви ввели 2");
                break;
            case 3:
                System.out.println("Варіант якщо ви ввели 3");
                break;
            case 4:
                System.out.println("Варіант якщо ви ввели 4");
                break;
            default:
                System.out.println("Число яке ви ввели не дорівнює ні 1 / ні 2 / ні 3 ");


        }
    }
}
